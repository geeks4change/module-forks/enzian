<?php

/**
 * @file
 * Functions to support theming in the enzian theme.
 */

/**
 * Implements hook_library_info_alter().
 * Copy from https://gitlab.com/drutopia/octavia/merge_requests/5/diffs
 */
function enzian_library_info_alter(&$libraries, $extension) {

}

/**
 * Implements hook_preprocess_HOOK() for html.html.twig.
 *
 * We need a stable body class for if map/organisation is frontpage or not (compare path- class in html template which is not stable)
 * See https://www.heididev.com/add-class-body-based-path-alias-drupal-8
 */
function enzian_preprocess_html(&$variables) {
  $current_path = \Drupal::service('path.current')->getPath();
  $path_alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
  $path_alias = ltrim($path_alias, '/');
  $variables['attributes']['class'][] = 'h4c-page-' . \Drupal\Component\Utility\Html::cleanCssIdentifier($path_alias);
}

/**
 * Implements hook_page_attachments_alter().
 */
function enzian_page_attachments_alter(array &$page) {
  // Tell IE to use latest rendering engine (not to use compatibility mode).
  /*$ie_edge = [
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => [
    'http-equiv' => 'X-UA-Compatible',
    'content' => 'IE=edge',
    ],
  ];
  $page['#attached']['html_head'][] = [$ie_edge, 'ie_edge'];*/
}

/**
 * Implements hook_preprocess_page() for page.html.twig.
 */
function enzian_preprocess_page(array &$variables) {

}

/**
 * Implements hook_theme_suggestions_page_alter().
 * Add Template Suggestions per Content Type.
 *
 * https://www.drupal.org/forum/support/theme-development/2015-07-02/how-to-add-in-drupal-8-a-custom-suggestion-page-template#comment-12127859
 * https://gitlab.com/geeks4change/hubs4change/enzian/issues/105
 */
function enzian_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  // Add content type suggestions.
  if (($node = \Drupal::request()->attributes->get('node')) && (strpos($_SERVER['REQUEST_URI'], "revisions") == false)) {
    array_splice($suggestions, 1, 0, 'page__node__' . $node->getType());
  }
}

/**
 * Implements hook_theme_suggestions_node_alter().
 */
function enzian_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  /*$node = $variables['elements']['#node'];

  if ($variables['elements']['#view_mode'] == "full") {

  }*/
}

/**
 * Implements hook_preprocess_HOOK().
 */
function enzian_preprocess_block(array &$variables) {

}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for field templates.
 *
 * Add entity reference target type template suggestion.
 * @see https://git.drupalcode.org/project/adaptivetheme/-/blob/8.x-3.x/at_core/includes/suggestions.inc
 *
 */
function enzian_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  // Add the entity reference type as a field template suggestion.
  if (isset($variables['element']['#items']) && is_object($variables['element']['#items'])) {
    $target_type = $variables['element']['#items']->getSetting('target_type') ?: NULL;
    if ($target_type !== NULL) {
      array_splice($suggestions, 1, 0, 'field__entity_reference_type__' . $target_type);
    }
  }
}

/**
 * Implements hook_theme_suggestions_field_alter().
 */
function enzian_theme_suggestions_fieldset_alter(array &$suggestions, array $variables) {
  /*$element = $variables['element'];
  if (isset($element['#attributes']['class'])
    && in_array('form-composite', $element['#attributes']['class'])) {
    $suggestions[] = 'fieldset__form_composite';
  }*/
}

/**
 * Implements hook_preprocess_node().
 */
function enzian_preprocess_node(array &$variables) {
  // Default to turning off byline/submitted.
  //$variables['display_submitted'] = FALSE;
}

/**
 * Implements hook_theme_suggestions_views_view_alter().
 */
function enzian_theme_suggestions_views_view_alter(array &$suggestions, array $variables) {

}

/**
 * Implements hook_preprocess_form().
 */
function enzian_preprocess_form(array &$variables) {
  //$variables['attributes']['novalidate'] = 'novalidate';
}

/**
 * Implements hook_preprocess_select().
 */
function enzian_preprocess_select(array &$variables) {
  //$variables['attributes']['class'][] = 'select-chosen';
}

/**
 * Implements hook_preprocess_details().
 */
function enzian_preprocess_details(array &$variables) {
  /*$variables['attributes']['class'][] = 'details';
  $variables['summary_attributes']['class'] = 'summary';*/
}

/**
 * Implements hook_theme_suggestions_details_alter().
 */
function enzian_theme_suggestions_details_alter(array &$suggestions, array $variables) {

}
