(function ($) {
  'use strict';
  $(document).on('leaflet.map', function (e, settings, lMap) {

    // Move popupPane out of map container according to https://gis.stackexchange.com/a/186169
    // Avoid overflow hidden issue in iOS/Safari which makes it unusable + works in IE11 (before the popup did not load)

    if ($('.js-h4c-popup').length) { /* Make sure to only hit on pages where we want fixed popups */

    // Get the popupPane
    var htmlObject = lMap.getPane('popupPane');
    // Move pane into main > section next to the other sidebar and main content area
    var a = document.querySelector('main > section');

    $(htmlObject).addClass('h4c-popup'); /* Because we move the element out of the parent container, we need to apply the class for styling again */
    // Finally append that node to the new parent, recursively searching out and re-parenting nodes.

    function setParent(el, newParent)
    {
      newParent.appendChild(el);
    }
    setParent(htmlObject, a);
    }
    // Fixed position popups seem not to play well with autopan.
    $(document).on('leaflet.feature', function (e, lFeature, feature, drupalLeaflet) {
      lFeature._popup.options.autoPan = false;
    });

  });

})(jQuery);

