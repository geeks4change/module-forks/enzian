(function ($, Drupal) {
  Drupal.behaviors.h4c_ie_fixedelements = {
    attach: function (context) {
      
      'use strict';

      // Only use this for IE, use browser hack to support IE 10/11
      // See https://gitlab.com/geeks4change/hubs4change/h4c/issues/369
      // Further https://www.drupal.org/project/drupal/issues/3095113 and http://browserhacks.com/#hack-a2481568d22565334dcf541979e164ac

      var isIE = document.body.style.msTouchAction !== undefined;

      if (isIE == true) {


        $('.h4c-sticky.h4c-menu--campaign--portal', context).once('h4c-menu--campaign--portal').each(function () {

          // We can only target one element per page atm. It would be better to use $(this), but I don't know how to pass $(this) into the scroll function
          var targetBlock = jQuery('.h4c-sticky.h4c-menu--campaign--portal');

          // Determine correct value to make elements fixed, see also top/margin.top applied in enzian/src/global/enzian/_navigation.scss#L17
          var makeFixed = targetBlock.offset().top - jQuery('.navbar').height();

          // Check if viewport is scrolled without the user having scrolled (e.g. pageload) and set class
          if (jQuery(window).scrollTop() > makeFixed) {
            targetBlock.addClass('h4c-fixed');
          }
          else {
            targetBlock.removeClass('h4c-fixed');
          }

          // Listen to scrolling and fix/remove fix element
          jQuery(window).scroll(function(event) {

            if (jQuery(window).scrollTop() > makeFixed) {
              targetBlock.addClass('h4c-fixed');
            }
            else {
              targetBlock.removeClass('h4c-fixed');
            }
          });
        });

        $('.h4c-sticky.contact-form', context).once('h4c-contact-form').each(function () {

          // We can only target one element per page atm. It would be better to use $(this), but I don't know how to pass $(this) into the scroll function
          var targetBlock = jQuery('.h4c-sticky.contact-form');

          // Determine correct value to make elements fixed, see also top/margin.top applied in enzian/src/global/enzian/_navigation.scss#L17
          var makeFixed = targetBlock.offset().top - jQuery('.navbar').height();

          // Check if viewport is scrolled without the user having scrolled (e.g. pageload) and set class
          if (jQuery(window).scrollTop() > makeFixed) {
            targetBlock.addClass('h4c-fixed');
          }
          else {
            targetBlock.removeClass('h4c-fixed');
          }

          // Listen to scrolling and fix/remove fix element
          jQuery(window).scroll(function(event) {

            if (jQuery(window).scrollTop() > makeFixed) {
              targetBlock.addClass('h4c-fixed');
            }
            else {
              targetBlock.removeClass('h4c-fixed');
            }
          });
        });

      }

    }
  };
})(jQuery, Drupal);
