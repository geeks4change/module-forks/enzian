/*eslint strict: ["error", "global"]*/
'use strict';

//=======================================================
// Include gulp
//=======================================================
var gulp = require('gulp');

//=======================================================
// Include Our Plugins
//=======================================================
var sync        = require('browser-sync');
var runSequence = require('run-sequence');
var gutil  = require('gulp-util');
var argv   = require('minimist')(process.argv);
var gulpif = require('gulp-if');
var prompt = require('gulp-prompt');
var rsync  = require('gulp-rsync');
var $ = require('gulp-load-plugins')();

//=======================================================
// Include Our tasks.
//
// Each task is broken apart to it's own node module.
// Check out the ./gulp-tasks directory for more.
//=======================================================
var taskCompile     = require('./gulp-tasks/compile.js');
var taskMove        = require('./gulp-tasks/move.js');
var taskLint        = require('./gulp-tasks/lint.js');
var taskCompress    = require('./gulp-tasks/compress.js');
var taskClean       = require('./gulp-tasks/clean.js');

var taskStyleGuide  = require('./gulp-tasks/styleguide.js');
var taskConcat      = require('./gulp-tasks/concat.js');

//=======================================================
// Compile Our Sass and JS
// We also move some files if they don't need
// to be compiled.
//=======================================================

// Compile Sass
gulp.task('compile:sass', function() {
  return taskCompile.sass();
});

// Compile JavaScript ES2015 to ES5.
gulp.task('compile:js', function() {
  return taskCompile.js();
});

// If some JS components aren't es6 we want to simply move them
// into the assets folder. This allows us to clean the assets/js
// folder on build.
gulp.task('move:js', function() {
  return taskMove.js();
});

gulp.task('compile', gulp.series(gulp.parallel('compile:sass', 'compile:js', 'move:js')));

//=======================================================
// Lint Sass and JavaScript
//=======================================================

// Lint Sass based on .sass-lint.yml config.
gulp.task('lint:sass', function () {
  return taskLint.sass();
});

// Lint JavaScript based on .eslintrc config.
gulp.task('lint:js', function () {
  return taskLint.js();
});

gulp.task('lint', gulp.series(gulp.parallel('lint:sass', 'lint:js')));

//=======================================================
// Compress Files
//=======================================================
gulp.task('compress', function() {
  return taskCompress.assets();
});

//=======================================================
// Generate style guide
//=======================================================
gulp.task('styleguide', function() {
  return taskStyleGuide.generate(__dirname);
});

//=======================================================
// Concat all CSS files into a master bundle.
//=======================================================
gulp.task('concat', function () {
  return taskConcat.css();
});

//=======================================================
// Clean all directories.
//=======================================================

// Clean style guide files.
gulp.task('clean:styleguide', function () {
  return taskClean.styleguide();
});

// Clean CSS files.
gulp.task('clean:css', function () {
  return taskClean.css();
});

// Clean JS files.
gulp.task('clean:js', function () {
  return taskClean.js();
});

gulp.task('clean', gulp.series(gulp.parallel('clean:css', 'clean:js', 'clean:styleguide')));

//=======================================================
// Watch and recompile sass.
//=======================================================

// Pull the sass watch task out so we can use run sequence.

gulp.task('watch:sass', function(callback) {
  runSequence(
    ['lint:sass', 'compile:sass'],
//    'concat',
    callback
  );
});

// Main watch task.
gulp.task('watch', function() {

  // BrowserSync proxy setup
  // Uncomment this and swap proxy with your local env url.
  // NOTE: for this to work in Drupal, you must install and enable
  // https://www.drupal.org/project/link_css. This module should
  // NOT be committed to the repo OR enabled on production.
  //
  // This should work out of the box for work within the style guide.
  //
  // sync.init({
  //   open: false,
  //   proxy: 'http://test.mcdev'
  // });

  // Watch all my sass files and compile sass if a file changes.
  gulp.watch(
    './src/{global,layout,components}/**/*.scss',
    ['watch:sass']
  );

  // Watch all my JS files and compile if a file changes.
  gulp.watch([
    './src/{global,layout,components}/**/*.js'
  ], ['lint:js', 'compile:js', 'move:js']);

  // Watch all my twig files and rebuild the style guide if a file changes.
  gulp.watch(
    './src/{layout,components}/**/*.twig',
    ['watch:styleguide']
  );

});


//
// Gulp Deploy to remote server
//

gulp.task('deploy', function() {
// Dirs and Files to sync
var rsyncPaths = ['/var/www/html/enzian/assets/**/*.css','/var/www/html/enzian/assets/**/*.css.map', '/var/www/html/enzian/assets/**/*.js', '/var/www/html/enzian/assets/_temp.css', '/var/www/html/enzian/src/**/*.html.twig' ];

// Default options for rsync
var rsyncConf = {
    progress: true,
    incremental: true,
    relative: true,
    emptyDirectories: true,
    recursive: true,
    clean: true,
    exclude: [],
  };

// Staging
  if (argv.staging) {

    rsyncConf.hostname = 'node-c0e4e528-b088-42dc-8f3a-581e5ac4d9a6.wod.by'; // hostname
    rsyncConf.username = 'wodby'; // ssh username
    rsyncConf.destination = '/var/www/html/web/themes/contrib/enzian/'; // path where uploaded files go
    rsyncConf.port = '31622'; // port

// Production
  } else if (argv.enzian) {

    rsyncConf.hostname = 'node-c0e4e528-b088-42dc-8f3a-581e5ac4d9a6.wod.by'; // hostname
    rsyncConf.username = 'wodby'; // ssh username
    rsyncConf.destination = '31404'; // path where uploaded files go

// Missing/Invalid Target
  } else {
    throwError('deploy', gutil.colors.red('Missing or invalid target'));
  }

// Use gulp-rsync to sync the files
  return gulp.src(rsyncPaths)
  .pipe(gulpif(
      argv.production,
      prompt.confirm({
        message: 'Heads Up! Are you SURE you want to push to PRODUCTION?',
        default: false
      })
  ))
  .pipe(rsync(rsyncConf));

});

function throwError(taskName, msg) {
  throw new gutil.PluginError({
      plugin: taskName,
      message: msg
    });
}

//
// Gulp drush clear cache
//
var themeName = 'enzian';
var config = {
  themeDir: '/themes/contrib' + themeName,
  drushPath: '/usr/local/bin/drush'
};
gulp.task('drushcr', function() {
  return gulp.src('', {
      read: false
    })
    .pipe($.shell([
      '/usr/local/bin/drush @wodby.mbm.hubs4change.andi3 cr',
    ]))
    .pipe($.notify({
      title: "Caches cleared",
      message: "Drupal caches cleared.",
      onLast: true
    }));
});

