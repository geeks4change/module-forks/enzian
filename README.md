# Enzian theme

## Dependencies
- drulma theme based on bulma
- drulma_companion which provides additional

## Limitations
- Bulma/drulma libraries are removed via library override and added back via `/src/global/bulma_sass/bulma_sass.scss` (and then added to global.scss)
- Bulma is installed in `node_modules` and can be updated via nvm
- As long as we compile bulma we cannot use cssvars consistently
  - We need to override possibly a lot of styles, see `src/components/_global/_bulma-overrides.scss`
  - Upstream there is [Feature: Support CSS Custom Properties (Variables) · Issue #1837 · jgthms/bulma · GitHub](https://github.com/jgthms/bulma/issues/1837)
- drulma_companion adds some custom block types which we currently use
  - Bulma navbar
  - Bulma tabs
- **Bulma navbar also adds site branding and an optional secondary menu**
  - Other drupal modules like `menu_block` are not compatible with it

## Browser Support

- Modern browsers that support cssgrid

## Usage

### Compile with gulp
- in theme dir run `gulp compile`
- to auto compile run `gulp watch`

## Usage and install instructions from enzian 1.0

*These instructions might be partly outdated*

If you haven't yet, install nvm:
https://github.com/creationix/nvm

#### Use the right version of node with:
`nvm use`

_This command will look at your `.nvmrc` file and use the version node.js specified in it. This ensures all developers use the same version of node for consistency._

#### If that version of node isn't installed, install it with:
`nvm install`

#### Install npm dependencies with
`npm install`

_This command looks at `package.json` and installs all the npm dependencies specified in it.  Some of the dependencies include gulp, autoprefixer, gulp-sass and others._

#### Runs default task
`npm run build`

_This will run whatever the default task is._

#### Compiles Sass
`npm run compile`

_This will perform a one-time Sass compilation._

#### Runs the watch command
`npm run watch`

_This is ideal when you are doing a lot of Sass changes and you want to make sure every time a change is saved it automatically gets compiled to CSS_

#### Cleans complied directory
`npm run clean`

_This will perform a one-time deletion of all compiled files within the dist/ directory._
