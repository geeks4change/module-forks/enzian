# 12.04.2020
- Also update H4C because of moved field templates in https://gitlab.com/geeks4change/hubs4change/enzian/-/commit/16acbaec7b6d0186785dd9cde40b1d49833f202d
  - When updating H4C you need to update config as well because of https://gitlab.com/geeks4change/hubs4change/h4c/-/issues/426

# 30.01.2020
- Color module => cssvars migration / if site uses color module:
  - Copy/past color values
  - Deinstall color
  - Install cssvars (colorng is not needed)
  - Add config:
  - For details see https://gitlab.com/geeks4change/hubs4change/enzian/commit/83c50ffbf6011a7bbda4515df653b40ddb46bbf8

`cssvars.enzian.yml`, optional per domain record: `domain.config.domain_record.cssvars.enzian.yml` and fill in color values:

```
text: '#222222'
link: '#ffffff'
primary_color: '#8db119'
primary_color_dark: '#8db119'
primary_bg: '#8db119'
primary_bg_dark: '#8db119'
content_bg: '#8db119'
header: ''
```